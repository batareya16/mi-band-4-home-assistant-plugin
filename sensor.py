from __future__ import annotations

from homeassistant.components.sensor import (
    SensorDeviceClass,
    SensorEntity,
    SensorStateClass,
)
from homeassistant.core import HomeAssistant
from homeassistant.helpers.entity_platform import AddEntitiesCallback
from homeassistant.helpers.typing import ConfigType, DiscoveryInfoType
from .connector import Connector
import argparse
import subprocess
from datetime import datetime


def setup_platform(
        hass: HomeAssistant,
        config: ConfigType,
        add_entities: AddEntitiesCallback,
        discovery_info: DiscoveryInfoType | None = None
) -> None:
    add_entities([StepsSensor(), BatteryInfoSensor(), GeneralInfoSensor(hass)])


class StepsSensor(SensorEntity):
    _attr_name = "Number of steps"
    _attr_state_class = SensorStateClass.MEASUREMENT
    _attributes = {'fat_burned': None, 'calories': None, 'distance_travelled': None}

    def update(self) -> None:
        binfo = Connector.getBand().get_steps()
        self._attr_native_value = binfo['steps']
        self._attributes['fat_burned'] = binfo['fat_burned']
        self._attributes['calories'] = binfo['calories']
        self._attributes['distance_travelled'] = binfo['meters']

    @property
    def extra_state_attributes(self):
        return self._attributes

    @property
    def unique_id(self):
        return "mi_band4_steps"


class BatteryInfoSensor(SensorEntity):
    _attr_name = "Battery level"
    _attr_state_class = SensorStateClass.MEASUREMENT

    def update(self) -> None:
        self._attr_native_value = Connector.getBand().get_battery_info()['level']

    @property
    def unique_id(self):
        return "mi_band4_battery"


class GeneralInfoSensor(SensorEntity):
    _attr_name = "Mi band information"
    _attr_state_class = SensorStateClass.MEASUREMENT
    _attributes = {'serial': None, 'hardware_revision': None, 'soft_revision': None}
    hass: HomeAssistant

    def __init__(self, hass):
        self.hass = hass
        Connector._default_music_play = lambda: self.fireEvent("miband_play")
        Connector._default_music_pause = lambda: self.fireEvent("miband_pause")
        Connector._default_music_back = lambda: self.fireEvent("miband_back")
        Connector._default_music_focus_in = lambda: self.fireEvent("miband_focusin")
        Connector._default_music_focus_out = lambda: self.fireEvent("miband_focusout")
        Connector._default_music_forward = lambda: self.fireEvent("miband_forward")
        Connector._default_music_vdown = lambda: self.fireEvent("miband_vdown")
        Connector._default_music_vup = lambda: self.fireEvent("miband_vup")

    def fireEvent(self, eventType) -> None:
        event_data = {
            "device_id": "mi_band4_info",
            "type": eventType,
        }
        self.hass.bus.async_fire("mi_band4_event", event_data)

    def update(self) -> None:
        band = Connector.getBand()
        self._attr_native_value = band.get_current_time()['date']
        self._attributes['serial'] = band.get_serial()
        self._attributes['hardware_revision'] = band.get_hrdw_revision()
        self._attributes['soft_revision'] = band.get_revision()

    @property
    def extra_state_attributes(self):
        return self._attributes

    @property
    def unique_id(self):
        return "mi_band4_info"
