"""Mi band 4 integration."""

from __future__ import annotations
from .constants import MUSICSTATE

import logging

from homeassistant.core import HomeAssistant, ServiceCall
from homeassistant.helpers.typing import ConfigType

# The domain of your component. Should be equal to the name of your component.
from .connector import Connector

DOMAIN = "mi_band4"
_LOGGER = logging.getLogger(__name__)


def setup(hass: HomeAssistant, config: ConfigType) -> bool:
    def send_notification(call: ServiceCall) -> None:
        title = call.data.get("title", "")
        message = call.data.get("message", "")
        Connector.getBand().send_custom_alert(5, title, message)

    hass.services.register(DOMAIN, 'send_miband_notification', send_notification)

    def send_missed_call(call: ServiceCall) -> None:
        phone = call.data.get("phone", "")
        title = call.data.get("title", "")
        Connector.getBand().send_custom_alert(4, phone, title)

    hass.services.register(DOMAIN, 'send_miband_missed_call', send_missed_call)

    def send_call(call: ServiceCall) -> None:
        phone = call.data.get("phone", "")
        title = call.data.get("title", "")
        Connector.getBand().send_custom_alert(3, phone, title)

    hass.services.register(DOMAIN, 'send_miband_call', send_call)

    def send_mail(call: ServiceCall) -> None:
        phone = call.data.get("mail", "")
        title = call.data.get("title", "")
        Connector.getBand().send_custom_alert(3, phone, title)

    hass.services.register(DOMAIN, 'send_miband_mail', send_mail)

    def set_music_state(call: ServiceCall) -> None:
        title = call.data.get("title", "")
        state = call.data.get("state", "played")
        Connector.getBand().setTrack(title, MUSICSTATE.PLAYED if state == "played" else MUSICSTATE.PAUSED)

    hass.services.register(DOMAIN, 'set_miband_music_state', set_music_state)
    return True
