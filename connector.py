from .constants import MUSICSTATE
from .miband import miband
import os
from threading import Thread
from time import sleep

MAC_ADDR = "E3:84:5E:58:21:74"
AUTH_KEY = bytes.fromhex("00ef70e47d0f42e3a65915529fc73a02")


class Connector:
    band: miband = None
    success = False
    refreshMusicStarted = False
    _default_music_play = None
    _default_music_pause = None
    _default_music_forward = None
    _default_music_back = None
    _default_music_focus_out = None
    _default_music_focus_in = None
    _default_music_vdown = None
    _default_music_vup = None
    counter = 0

    @staticmethod
    def refreshMusic():
        while True:
            try:
                Connector.getBand()
            finally:
                sleep(1)

    @staticmethod
    def getBand():
        try:
            Connector.band.enable_music()
        except:
            try:
                Connector.initializeBand()
            except:
                Connector.restartBluetoothService()
                Connector.initializeBand()
                print('1')
        finally:
            return Connector.band

    @staticmethod
    def restartBluetoothService():
        os.system("sudo hciconfig hci0 reset")
        sleep(2)

    @staticmethod
    def initializeBand():
        Connector.band = miband(MAC_ADDR, AUTH_KEY, debug=False)
        initialized = Connector.band.initialize()
        if initialized:
            Connector.band.setTrack("Home assistant", MUSICSTATE.PLAYED)
            Connector.band.setMusicCallback(
                Connector._default_music_play,
                Connector._default_music_pause,
                Connector._default_music_forward,
                Connector._default_music_back,
                Connector._default_music_vup,
                Connector._default_music_vdown,
                Connector._default_music_focus_in,
                Connector._default_music_focus_out)
            if not Connector.refreshMusicStarted:
                Connector.refreshMusicStarted = True
                Thread(target=Connector.refreshMusic).start()
            Connector.success = True
